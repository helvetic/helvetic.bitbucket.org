/*!
npm install gulp gulp-watch gulp-autoprefixer gulp-uglify gulp-sourcemaps gulp-imagemin imagemin-pngquant gulp-clean gulp-livereload gulp-notify gulp-cache gulp-if gulp-rename gulp-concat gulp-include gulp-html-replace gulp-postcss postcss-flexibility postcss-import gulp-css-url-adjuster gulp-changed gulp-preprocess--save-dev
npm install  --save-dev
*/
'use strict';

var gulp = require('gulp'),
  watch = require('gulp-watch'),
  prefixer = require('gulp-autoprefixer'),
  uglify = require('gulp-uglify'),
  sourcemaps = require('gulp-sourcemaps'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  clean = require('gulp-clean'),
  livereload = require('gulp-livereload'),
  notify = require('gulp-notify'),
  cache = require('gulp-cache'),
  _if = require('gulp-if'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  include = require("gulp-include"),
  htmlreplace = require("gulp-html-replace"),
  postcss = require("gulp-postcss"),
  cssimport = require("postcss-import"),
  urlAdjuster = require('gulp-css-url-adjuster'),
  changed = require('gulp-changed'),
  preprocess = require('gulp-preprocess');


var production = false;
//куда собирается всё это дело
if(production){
  var build_path = 'dist/';
}else{
  var build_path = 'build/';
}


var path = {
  build: {
    html: build_path,
    js: build_path + 'js/',
    css: build_path + 'css/',
    img: build_path + 'img/',
    fonts: build_path + 'fonts/'
  },
  src: {
    root: 'src',
    html: 'src/*.html',
    js: 'src/js/script.js',
    scripts: 'src/js/**/*.*',
    style: 'src/css/style.css',
    stylemap: 'src/css/',
    stylecss: 'src/css/style.css',
    css: 'src/css/**/*.*',
    img: 'src/img/**/*.*',
    img_ignore: 'src/img/ignore/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  watch: {
    html: 'src/**/*.html',
    js: 'src/js/**/*.js',
    css: 'src/css/**/*.*',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  clean: {
    child: './' + build_path + '**',
    self: '!./' + build_path
  }
};

gulp.task('html', function () {
  gulp.src(path.src.html)
    .pipe(preprocess({
      context: {
        NODE_ENV: 'production',
        DEBUG: true
      }
    }))//To set environment variables in-line
      .on('error', console.log)
    .pipe(_if(production,htmlreplace({
         'css': 'css/style.css',
     })))
    .pipe(gulp.dest(path.build.html))
    .pipe(livereload());

  /*gulp.src(path.src.html)
    .pipe(include())
      .on('error', console.log)
    .pipe(_if(production,htmlreplace({
         'css': 'css/style.css',
     })))
    .pipe(gulp.dest(path.build.html))
    .pipe(livereload());*/
});


/*gulp.task('js', function () {
  gulp.src(path.src.js)
    .pipe(concat('script.js'))
    .pipe(gulp.dest(path.build.js))
    .pipe(rename({suffix: '.min'}))
    .pipe(_if(production,uglify()))
    .pipe(gulp.dest(path.build.js))
    .pipe(livereload());
});*/

gulp.task('js', function () {
  gulp.src(path.src.scripts)
    .pipe(gulp.dest(path.build.js))
    .pipe(livereload());
});

gulp.task('css', function () {
  if(production){
    gulp.src(path.src.stylecss)
      .pipe(postcss([
        require("postcss-import")(),
      ]))
      .pipe(postcss([
        require("postcss-flexibility")(),
      ]))
      .pipe(urlAdjuster({
        replace:  ['/src/img','../img/'],
      }))
      .pipe(gulp.dest(path.build.css));
  }else{
    /*gulp.src(path.src.css)
      .pipe(urlAdjuster({
        prependRelative: build_path + '/img/',
      }))
      .pipe(gulp.dest(path.build.css));*/
  }

});

/*gulp.task('css', function () {
  gulp.src(path.src.css)
});*/

gulp.task('image', function () {
  gulp.src([path.src.img, path.src.img_ignore])
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()],
      interlaced: true
    }))
    .pipe(gulp.dest(path.build.img))
    .pipe(livereload());
});

gulp.task('font', function() {
  gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});



gulp.task('clean', function() {
  /*del([path.clean.child, path.clean.self]);*/
  return gulp.src(build_path, {read: false})
		.pipe(clean());
});


gulp.task('build', ['clean'],function() {
  gulp.start(
  'html',
  'js',
  'css',
  'font',
  'image'
    );
});
gulp.task('buildf', function() {
  gulp.start(
  'html',
  'js',
  'css',
  'font'
    );
});


gulp.task('watch', function(){
  livereload.listen();
  gulp.watch(path.watch.html, ['html']);
  gulp.watch(path.watch.css, ['css']);
  //gulp.watch(path.watch.style, ['style']);
  gulp.watch(path.watch.js, ['js']);
  //gulp.watch(path.watch.image, ['image']);
  //gulp.watch(path.watch.font, ['font']);

});

gulp.task('default', ['build', 'watch']);



function onError(err) {
  console.log(err);
  this.emit('end');
}
