$(function(){

  $('.map-zoom-in').on('click', function(e){
    e.preventDefault();
    var zoom = map.getZoom();
    map.setZoom(zoom + 1);
  });

  $('.map-zoom-out').on('click', function(e){
    e.preventDefault();
    var zoom = map.getZoom();
    map.setZoom(zoom - 1);
  });

  $('.map-offers-expand').on('click', function(e){
    e.preventDefault();
    $(this).parent().find('.map-offers').toggleClass('active');
  });


  /* прокрутка предложений */
  function scrollCards(animate){
    var cardList = $('.card-mini-list'),
        current = $('.card-mini.active'),
        cardWidth = current.outerWidth(),
        listWidth = cardList.outerWidth();
        cardsBefore = current.index('.card-mini'),
        scrollPos = (cardWidth + 10) * cardsBefore - ((listWidth - cardWidth) / 2) + 100;

    if(animate){
      cardList.animate({
        scrollLeft: scrollPos
      }, 200);
    }else{
      cardList.scrollLeft(scrollPos);
    }
  }

  scrollCards();


  $('.card-mini-slide.next').on('click', function(e){
    e.preventDefault();
    if($('.card-mini.active').next('.card-mini').length){
      $('.card-mini.active').removeClass('active')
        .next('.card-mini').addClass('active');
      scrollCards(true);
    }
  });

  $('.card-mini-slide.prev').on('click', function(e){
    e.preventDefault();
    if($('.card-mini.active').prev('.card-mini').length){
      $('.card-mini.active').removeClass('active')
        .prev('.card-mini').addClass('active');
      scrollCards(true);
    }
  });



/* Демо первый вартиант прокрутки предложений*/
/*  $('.card-mini-slide.next').on('click', function(e){
    e.preventDefault();

    var current = $(this).parent().find('.card-mini.active'),
        prev = $(this).parent().find('.card-mini.prev'),
        next = $(this).parent().find('.card-mini.next'),
        last = $(this).parent().find('.card-mini.last');

    prev.removeClass('prev');

    if(next.next('.card-mini').length){
      current.addClass('prev').removeClass('active');
      next.addClass('active').removeClass('next')
        .next('.card-mini').addClass('next');
    }else{
      current.removeClass('active next');
      last.addClass('active')
        .prev().addClass('prev');
    }

  });


  $('.card-mini-slide.prev').on('click', function(e){
    e.preventDefault();

    var current = $(this).parent().find('.card-mini.active'),
        prev = $(this).parent().find('.card-mini.prev'),
        next = $(this).parent().find('.card-mini.next'),
        first = $(this).parent().find('.card-mini.first');

    next.removeClass('next');

    if(prev.prev('.card-mini').length){
      current.addClass('next').removeClass('active');
      prev.addClass('active').removeClass('prev')
        .prev('.card-mini').addClass('prev');
    }else{
      current.removeClass('active prev');
      first.addClass('active')
        .next().addClass('next');
    }

  });*/




  /* ДЕМО сворачивание формы поиска */
  $('.map-search-expand-btn').on('click', function(e){
    e.preventDefault();
    $(this).empty().append('Фильтр-поиск' + '<span class="icon-arrow-down"></span>');
    $(this).parents('.map-search').removeClass('active');
  });


});
