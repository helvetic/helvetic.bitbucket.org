$(function(){

  $( ".spinner-plus" ).spinner({
    min: 0,
    icons: { down: "icon-minus", up: "icon-plus-light" }
  });

});
