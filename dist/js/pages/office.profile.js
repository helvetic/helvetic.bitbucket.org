$(function(){
  
  $('#office-form-country').selectize({
      create: false,
      sortField: 'id',
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      options: [
          {id: 1, title: 'Россия'},
          {id: 2, title: 'Польша'},
          {id: 3, title: 'Латвия'},
          {id: 4, title: 'Литва'},
          {id: 5, title: 'Эстония'},
      ]
  });

  $('.input-add-btn').inputOnemore();

});
