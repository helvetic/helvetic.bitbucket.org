$(function(){



  /* таблица */
  $('.a-table').DataTable({
    paging:   false,
    ordering: false,
    info:     false,
    searching: false,
    rowReorder: {
        selector: '.draggable'
    }
  });


  /* инпуты количества */
  $( ".spinner" ).spinner({
    min: 0
  });

  /* время */
  $('.clockpicker>input').mask('00:00');
  $('.clockpicker>input').clockpicker({
    autoclose:true
  });

  /* акции */
  $('select.tickets-share-select').selectize({
      create: false
  });
  $('body').on('shown.bs.dropdown','.tickets-table .dropdown-container', function(){
    $(this).find('textarea').focus();
  })


  /* доп услуги */
  function addTextable(e){
    e.preventDefault();
    var input = '<div class="input-removable input-textable">' +
                '<input type="text" value="" placeholder="Дополнительная услуга">' +
                '<button class="input-remove"><span class="icon-x-light"></span></button>' +
                '</div>',
        parent = $(e.target).parents('.a-table-content').find('.tickets-table-services');
    parent.append(input).find('input').focus();
  }
  /* добавление доп услуги*/
  $('body').on('click touchstart', '.tickets-table-services-add', addTextable);
  $('body').on('keypress', '.tickets-table-services input', function(e){
    if(e.which == 13) {
      addTextable(e);
    }
  });
  /* удаление доп услуги*/
  $('body').on('click touchstart', '.input-removable .input-remove',function(e){
    e.preventDefault();
    $(this).parent().remove();
  });
  $('body').on('focus','.input-textable input', function(){
    $(this).select();
  });




  /* Удаление строки таблицы */
  $('body').on('click touchstart', '.ticket-close', function(e){
    e.preventDefault();
    $(this).parents('.tickets-table-item').remove();
  });


  /* ДЕМО */

  /* Копирование строки таблицы (время копировать не нужно)*/
  $('body').on('click touchstart', '.ticket-copy', function(e){
    e.preventDefault();
    var tbody = $(this).parents('tbody')
    var ticket_clone = $(this).parents('.tickets-table-item').clone();
    ticket_clone.find('.clockpicker input').val('');
    tbody.append(ticket_clone);
  });

  /* удаление загруженного xls */
  $('body').on('click', '.form-file-doc-remove', function(e){
    e.preventDefault();
    $(this).parents('.form-file').removeClass('form-file-full');
    $(this).parents('.form-file-doc').remove();
  });

});
