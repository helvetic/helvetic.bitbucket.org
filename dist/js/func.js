function loadSVG(path){
  var file     = path || 'img/svg.svg',
       revision = 1;
   if( !document.createElementNS || !document.createElementNS( 'http://www.w3.org/2000/svg', 'svg' ).createSVGRect )
       return true;
   var isLocalStorage = 'localStorage' in window && window[ 'localStorage' ] !== null,
       request,
       data,
       insertIT = function(){
           document.body.insertAdjacentHTML( 'afterbegin', data );
       },
       insert = function(){
           if( document.body ) insertIT();
           else document.addEventListener( 'DOMContentLoaded', insertIT );
       };

   if( isLocalStorage && localStorage.getItem( 'inlineSVGrev' ) == revision ){
       data = localStorage.getItem( 'inlineSVGdata' );
       if( data ){
           insert();
           return true;
       }
   }

   try{
       request = new XMLHttpRequest();
       request.open( 'GET', file, true );
       request.onload = function(){
           if( request.status >= 200 && request.status < 400 ){
               data = request.responseText;
               insert();
               if( isLocalStorage ){
                   localStorage.setItem( 'inlineSVGdata',  data );
                   localStorage.setItem( 'inlineSVGrev',   revision );
               }
           }
       }
       request.send();
   }
   catch( e ){}
}




function twoDigits(j) {
  return ('0' + j).slice(-2);
}


function checkWidth(){
  var curWidth = window.innerWidth,
      result;
  if(curWidth >= 1200){
    result = 4;
  }else if(curWidth >= 992){
    result = 3;
  }else if(curWidth >= 768){
    result = 2;
  }else{
    result = 1;
  }
  return result;
  console.log(result);
}

$.fn.outerHTML = function() {
  return $('<div />').append(this.eq(0).clone()).html();
};
