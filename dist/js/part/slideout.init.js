var slideout = new Slideout({
   'panel': document.getElementById('panel'),
   'menu': document.getElementById('menu'),
   'padding': 269,
   'tolerance': 70,
   'side': 'right'
});

$('body').on('click', '.slideout-panel[style*="transform"]', function(e){
  slideout.close();
});
