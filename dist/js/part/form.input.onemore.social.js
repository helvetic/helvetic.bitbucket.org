$(function(){


  /* Добавление полей для ссылок на соцсети */
  $('body').on('click', '#social-links-add .social-btns .btn', function(e){
    e.preventDefault();

    var socName = $(this).data('social-name'),
        socHtml = '<div class="input-removable">' +
                  '<label for="' + socName + '">' + $(this).html() + '</label>' +
                  '<input type="text" class="input-sm" name="' + socName + '" id="' + socName + '">\n\r' +
                  '<button class="input-remove"><span class="icon-x-light"></span></button>' +
                  '</div>';

    $('#social-links-add').prepend(socHtml);
    $(this).remove();
  });


  /* Удаление */
  $('body').on('click', '#social-links-add .input-remove', function(e){
    e.preventDefault();

    var socInput = $(this).prev('input'),
        socName = socInput.attr('name'),
        socIcon = socInput.prev('label').html(),
        socHtml = '<button class="btn btn-bordered ' + socName + '" data-social-name="' +
                  socName + '">' +
                  socIcon + '</button>';

    $('#social-links-add .social-btns').append(socHtml);

    $(this).parent().remove();
  });




});
