
/* позиционирование слайдового меню между верхней границей и футером */
function SlideMenu(menu){
  this.menu = menu;
  this.panelOffset       = menu.parent().offset().top,
  this.panelHeight       = menu.parent().height(),
  this.panelBottomOffset = this.panelOffset + this.panelHeight,
  this.menuHeight        = menu.height(),
  this.footerHeight      = $('.footer').height();
  this.setPosition();
  return this;
}

SlideMenu.prototype.setPosition = function () {
  this.menuOffset = this.menu.offset().top;
  if(this.menuOffset + $( window ).height() >= this.panelBottomOffset){
    if(this.menuOffset - this.panelOffset < 0){
      this.menu.css('top', 0);
    }else{
      this.menu.css('top', this.menuOffset - this.panelOffset);
    }
    this.menu.addClass('absolute');
  }else{
    this.menu.removeClass('absolute');
    if(this.panelOffset > this.menuOffset){
      this.menu.css('top', this.panelOffset - this.menuOffset);
    }else{
      this.menu.css('top', 0);
    }
  }
}
