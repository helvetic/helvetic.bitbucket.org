$(function(){

  /* Поворот изображения. Возвращает угол поворота по часовой*/
  function rotateImages() {
    var angle = 0;
    $(document).on('click', '.form-file-img-rotate', function(e){
      e.preventDefault();
      angle = (angle + 90) % 360;
      $(this).parent().find('img').removeClass();
      $(this).parent().find('img').addClass('img-rotate-' + angle);
      return angle;
    });

  }
  rotateImages();



  /* Код ниже для примера*/
  $.fn.loadfileExample = function(options) {

    var defaults = {
      multi: false,
      hideBtn: true
    };

    var settings = $.extend( {}, defaults, options );


    return this.each(function() {
      $(this).change(function(){
        placeImage(this,settings);
      });
    });

  };


  function placeImage(input,settings) {

    if (input.files && input.files[0]) {

      for (var i = 0; i < input.files.length; i++) {
        var file = input.files[i];

        if (!file.type.match('image')) continue;

        var reader = new FileReader(),
            parent = $(input).parents('.form-file'),
            imagesContainer = parent.find('.form-file-images');

        reader.onload = function (e) {
          var image = '<div class="form-file-img">' +
                      '<button class="form-file-img-rotate"><span class="icon-rotate"></span></button>' +
                      '<button class="form-file-img-close"><span class="icon-x-small"></span></button>' +
                      '<img src="' + e.target.result + '" alt="">' +
                      '</div>';
          if(imagesContainer.find('.form-file-btn').length){
            imagesContainer.find('.form-file-btn').before(image);
          }else{
            imagesContainer.append(image);
          }
          parent.addClass('form-file-full');
        }


        reader.readAsDataURL(file);
      }
    }
  }

  $(document).on('click', '.form-file-img-close', function(e){
    e.preventDefault();
    var mainContainer = $(this).parents('.form-file');
        imgContainer = $(this).parents('.form-file-images');

    $(this).parent().remove();

    if(!($.trim(imgContainer.html()))){
      mainContainer.removeClass('form-file-full');
    }
  });



});
