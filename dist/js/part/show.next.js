$(function(){
  $('body').on('click','.show-next', function(e){
    e.preventDefault();
    $(this).next('.show-next-content').toggleClass('active');
    if($(this).hasClass('show-next-hide')){
      $(this).toggleClass('show-next-hidden');
    }
    $(this).next('.show-next-content').find('input,textarea').focus();
  });
});
