$(function(){


  $('.r-table').each(function(){
    var table = $(this),
        table_head = table.find('.r-table-head'),
        table_body = table.find('.r-table-body');

    /* прокрутка таблицы в конец */
    table_body.scrollTop(table_body.prop('scrollHeight'));

    /* FIX разная ширина шапки и тела таблицы из-за скроллбара */
    if(table_body.find('tr').length){
      var tr_width = table_body.find('tr').width();
      table_head.find('tr').width(tr_width);
    }
  });


});
