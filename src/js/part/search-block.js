/* блок поиска */

/* выбор города */
/* можно сделать с помощью <select><option.. , можно с помощью объектов в свойстве options*/
$('#search-form-loc').selectize({
    create: false,
    sortField: 'id',
    valueField: 'id',
    labelField: 'title',
    searchField: 'title',
    options: [
        {id: 1, title: 'Санкт-Петербург'},
        {id: 2, title: 'Пярну'},
        {id: 3, title: 'Таллин'},
        {id: 4, title: 'Бачка-Таполо'},
        {id: 5, title: 'Париж'},
        {id: 6, title: 'Лондон'},
        {id: 7, title: 'Монако'},
        {id: 8, title: 'Мадрид'},
        {id: 9, title: 'Неаполь'},
    ]
});

/* выбор спорта */
$('#search-form-sport').selectize({
    create: false,
    sortField: 'id',
    valueField: 'id',
    labelField: 'title',
    searchField: 'title',
    options: [
        {id: 1, title: 'Скейтборд'},
        {id: 2, title: 'Горные лыжи'},
        {id: 3, title: 'Сноуборд'},
        {id: 4, title: 'Аэротруба'},
        {id: 5, title: 'BMX'},
        {id: 6, title: 'Дельтаплан'},
        {id: 7, title: 'Катание на лошадях'},
        {id: 8, title: 'Альпинизм'},
        {id: 9, title: 'Дайвинг'},
    ],
    plugins: ['remove_button']
});

/* календарик в поиске*/
$('.form-date').datepicker({
  showOtherMonths: true,
  selectOtherMonths: true,
  minDate: new Date(),
  onSelect: function(dateText, inst) {
    var dateAsObject = $(this).find('.date-day').val(twoDigits(inst.selectedDay));
    var dateAsObject = $(this).find('.date-month').val(twoDigits(inst.selectedMonth));
    var dateAsObject = $(this).find('.date-year').val(inst.selectedYear);
    $(this).removeClass('active');
    console.log(inst);
  }
});
$('.form-date .form-date-pick').on('click', function(e){
  e.preventDefault();
  $(this).parent().toggleClass('active');
});

$('.form-date-reset').on('click', function(e){
  e.preventDefault();
  $(this).parent().find('input').val('');
});
