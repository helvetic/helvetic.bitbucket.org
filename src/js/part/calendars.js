$(function(){

  if($(document).width() < 768){
    var m_num = 1;
  }else{
    var m_num = 2;
  }
  $( "#calendars-month" ).datepicker({
    numberOfMonths: m_num,
    showOtherMonths: true,
    selectOtherMonths: true,
    minDate: new Date(),
  });


  /* пример работы календаря недель */
  $('.week-calendar-controls>*').on('click', function(e){
    e.preventDefault();
    var tbody = $(this).parents('.week-calendar').find('tbody'),
        current_tr = tbody.find('tr.current'),
        direction = $(this).data('direction');

    tbody.addClass('sliding');

    if(direction == 'next'){
      var activated_tr = current_tr.next('tr');
    }else{
      var activated_tr = current_tr.prev('tr');
    }

    if(activated_tr.length){
      activated_tr.addClass('current');
      current_tr.removeClass('current').addClass(direction);
      setTimeout(function(){
        current_tr.removeClass(direction);
      }, 200);
    }

    setTimeout(function(){
      tbody.removeClass('sliding');
    }, 200);
  });

  $('.week-calendar-table button').on('click', function(e){
    e.preventDefault();
    $('.week-calendar-table button').removeClass('current');
    $(this).addClass('current');
  });


});
