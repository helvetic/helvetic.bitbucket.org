$(function(){


  $.fn.inputOnemore = function(options) {

    var defaults = {
      social: false
    };

    var settings = $.extend( {}, defaults, options );



    return this.each(function() {
      var input = $(this).parent().find('input:first');

      $(this).on('click', function(e){
        e.preventDefault();
        addInput(input);
      });

    });


  };

  function addInput(input){
    var lastInput = $(input).parent().find('input:last'),
        lastContainer = lastInput.parent(),
        newContainer = input.parent().clone(),
        newInput = newContainer.find('input');
    newContainer.empty();

    /* манипуляции с новым инпутом, например изменение name */
    var regexp = /[\d]+$/,
        oldName = newInput.attr('name'),
        replaceble =  parseInt(regexp.exec(oldName)),
        newName = oldName.replace(regexp, replaceble + 1);
    newInput.attr('name', newName);

    lastContainer.after(newContainer);
    newContainer.append(newInput);
    newContainer.append('<button href="#" class="input-remove">' +
                        '<span class="icon-x-light"></span>' +
                        '</button>');
  }





  $('body').on('click', '.input-remove', function(e){
    e.preventDefault();
    $(this).parent().remove();
  });




});
