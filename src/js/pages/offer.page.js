$(function(){
  /* инпуты количества */
  $( ".spinner-plus" ).spinner({
    min: 0,
    icons: { down: "icon-minus", up: "icon-plus-light" }
  });

  $('.zoom-img').magnificPopup({
    type: 'image',
    closeOnContentClick: true,
    showCloseBtn: false
  });

  /* подсказка */
   $('[data-toggle="tooltip"]').tooltip({
     container: 'body'
   })

});
