$(function(){

  /* ДЕМО галерея в карточке */
  $('.card-gallery-img')
    .on('mouseenter', function(){
      var image = $(this).data('image');
      var result = '<div class="card-gallery-img-zoom" style="background-image:url(' + image + ')"></div>';
      $(this).append(result);
    })
    .on('mouseleave', function(){
      $(this).empty();
    });

  $('body').on('mouseenter', '.card-gallery-img-zoom', function(){
    $(this).remove();
    console.log('ds');
  });


});
