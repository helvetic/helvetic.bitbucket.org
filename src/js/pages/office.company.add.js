$(function(){

  $('#office-form-country').selectize({
      create: false,
      sortField: 'id',
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      options: [
          {id: 1, title: 'Россия'},
          {id: 2, title: 'Польша'},
          {id: 3, title: 'Латвия'},
          {id: 4, title: 'Литва'},
          {id: 5, title: 'Эстония'},
      ]
  });

  $('#office-form-entity').selectize({
      create: false,
      sortField: 'id',
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      options: [
          {id: 1, title: 'ЗАО (Закрытое Акционерное Общество)'},
          {id: 2, title: 'ОАО (Открытое Акционерное Общество)'},
          {id: 3, title: 'ООО (Общество с Ограниченной Ответственностью)'},
      ]
  });


  $('#load-logo').loadfileExample();
  $('.input-add-btn').inputOnemore();


});
