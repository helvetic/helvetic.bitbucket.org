$(function(){

  $('#office-form-sport').selectize({
      create: false,
      sortField: 'id',
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      options: [
          {id: 1, title: 'Сноуборд'},
          {id: 2, title: 'Лыжи'},
          {id: 3, title: 'Дельтаплан'},
          {id: 4, title: 'Дайвинг'},
          {id: 5, title: 'Альпинизм'},
      ]
  });

  $('#office-form-country').selectize({
      create: false,
      sortField: 'id',
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      options: [
          {id: 1, title: 'Россия'},
          {id: 2, title: 'Польша'},
          {id: 3, title: 'Латвия'},
          {id: 4, title: 'Литва'},
          {id: 5, title: 'Эстония'},
      ]
  });


  $('#office-form-region').selectize({
      create: true,
      sortField: 'id',
      valueField: 'id',
      labelField: 'title',
      searchField: 'title',
      options: [
          {id: 1, title: 'Город1'},
          {id: 2, title: 'Город2'},
          {id: 3, title: 'Город3'},
          {id: 4, title: 'Город4'},
          {id: 5, title: 'Город5'},
      ]
  });

  $('#load-photos').loadfileExample({
    multi: true,
    hideBtn: false
  });
  $('.input-add-btn').inputOnemore();

});
