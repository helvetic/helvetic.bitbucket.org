$(function(){


  /* inline textarea */
  function adjustTextareaHeight(){
    $(this).height(24);
    $(this).height(this.scrollHeight);
    console.log(this.scrollHeight);
  }
  $('.input-textable textarea').each(adjustTextareaHeight);
  $('body')
    .on('ready input change','.input-textable textarea', adjustTextareaHeight)
    .on('focus','.input-textable textarea', function(){
      $(this).select();
    });
  /* кнопка редактировать название */
  $('body').on('click', '.form-file-doc-edit', function(e){
    e.preventDefault();
    $(this).parents('.form-file-doc').find('textarea').focus();
  });



  /* ДЕМО */

  $('body').on('click', '.form-file-doc-remove', function(e){
    e.preventDefault();
    $(this).parents('.form-file-doc').remove();
  });

});
