$(function(){

  if($(window).width()<768){
    var isMobile = true;
  }


  /* загрузка СВГ*/
  loadSVG('img/svg.svg');



  /* держать dropdown(выбор страны/города) открытым */
  $('.keep-open').on('click', function(e){
    e.stopPropagation();
  });



  /* закрытие окна выбора города при нажатии на кнопку */
  $('#select-location-submit').on('click', function(e){
    $('#select-location').find('.dropdown-toggle').trigger('click');
  });



  /* рейтинг */
  $('.rating span').on('click', function(){
    $(this).parent().find('.rated').removeClass('rated');
    $(this).addClass('rated');
  });



  /* сердечко */
  $('body').on('click', '.favorite-btn', function(e){
    $(this).toggleClass('active');
  });




});
